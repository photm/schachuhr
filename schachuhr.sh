#!/bin/bash

# schachuhr, A clock for playing time-limited chess
# Copyright (C) 2017  shak-mar
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

initial_mins=${1-5}
initial_player=${2-left}

usage() {
    echo "Usage: $0 [INITIAL_MINUTES [INITIAL_PLAYER]]"
    echo INITIAL_MINUTES is the number of minutes the players have each
    echo 'INITIAL_PLAYER is either "left" or "right" depending on'
    echo '  which player should start'
    exit 2
}

[[ $# > 2 ]] && usage
[[ $initial_player != left && $initial_player != right ]] && usage

seconds_to_human_readable_time() {
    local seconds="$1"
    date --utc -Iseconds -d "@$seconds" | sed s/1970-01-01T// | sed 's/+.*//'
}

minute=60

left_timer=$((initial_mins * minute))
right_timer=$((initial_mins * minute))

left_turns=0
right_turns=0

current_timer=$initial_player
toggle_timer() {
    if [[ $current_timer == left ]]; then
        current_timer=right
    else
        current_timer=left
    fi
}

decr_timer() {
    if [[ $current_timer == left ]]; then
        left_timer=$((left_timer - 1))
    else
        right_timer=$((right_timer - 1))
    fi
}

incr_turns() {
    if [[ $current_timer == left ]]; then
        left_turns=$((left_turns + 1))
    else
        right_turns=$((right_turns + 1))
    fi
}

underline=$(seconds_to_human_readable_time 0 | tr :0-9 =)
display_timers() {
    local left=$(seconds_to_human_readable_time $left_timer)
    local right=$(seconds_to_human_readable_time $right_timer)
    tput cup 0 0
    echo "$left | $right"
    if [[ $current_timer != left ]]; then
        echo -n "$(echo $underline | tr = ' ')   "
    fi
    echo $underline
    echo -n "$(echo $underline | tr = ' ')"
    echo -n " | $right_turns"
    tput cup 2 0
    echo $left_turns
}

clear

while [[ $left_timer > 0 && $right_timer > 0 ]]; do
    if read -t1 -n1; then
        incr_turns
        toggle_timer
        clear
    else
        decr_timer
    fi
    display_timers
done
